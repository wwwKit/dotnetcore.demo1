﻿using core.Bll;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace core.demo.Controllers
{

    public class testController : Controller
    {
        [HttpGet]
        public JsonResult test(int id)
        {
            var list = new s_userBll().GetUserByUserId(id);

            return Json(new { list = list });
        }
    }
}
