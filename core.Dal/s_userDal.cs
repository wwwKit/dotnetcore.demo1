﻿using core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.Dal
{
    public class s_userDal : DbContext<s_user>
    {

        public List<s_user> GetUserByUserID(int tid)
        {
            return Db.Queryable<s_user>()
                .Where(q => q.tid == tid)
                .ToList();
        }
    }
}
