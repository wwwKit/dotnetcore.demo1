﻿using core.Dal;
using core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.Bll
{
    public class s_userBll
    {
        public List<s_user> GetUserByUserId(int tid)
        {
            return new s_userDal().GetUserByUserID(tid);
        }
    }
}
