﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.Model
{
    [SugarTable("s_user")]
    public class s_user
    {
        [SugarColumn(ColumnDataType = "int", ColumnName = "tid", IsPrimaryKey = true, IsIdentity = true)]
        public int tid { get; set; }

        [SugarColumn(ColumnDataType = "varchar", ColumnName = "username")]
        public string username { get; set; }

        [SugarColumn(ColumnDataType = "varchar", ColumnName = "password")]
        public string password { get; set; }

        [SugarColumn(ColumnDataType = "varchar", ColumnName = "name")]
        public string name { get; set; }

        #region 扩展属性
        [SugarColumn(IsIgnore = true)]
        public int age { get; set; }
        #endregion
    }
}
